'use strict';

/**
 * precious-data.js controller
 *
 * @description: A set of functions called "actions" of the `precious-data` plugin.
 */


module.exports = {

  import: async (ctx) => {
    // @todo init the import
    ctx.send({ yolo: 'yes' })
  },

  sbtpCardCount: async (ctx) => {
    const services = strapi.plugins['precious-data'].services['precous-data']
    const sbtpCardCount = services.getSbtpCardCount()
    ctx.send({ sbtpCardCount })
  },

  /**
   * Get the paths on disk to the prememo card json files
   *
   * @return {Object}
   */

  paths: async (ctx) => {

    let getJsonPaths = strapi.plugins['precious-data'].services['precious-data'].getJsonPaths
    let getSbtpCardCount = strapi.plugins['precious-data'].services['precious-data'].getSbtpCardCount
    let jsonPaths, sbtpCardCount

    try {
      jsonPaths = await getJsonPaths()
      sbtpCardCount = await getSbtpCardCount()
    } catch (e) {
      console.log('problum')
      console.log(e)
      return ctx.badRequest(null, [{ messages: [{ id: 'your code is fucked!' }] }]);
    }

    // Send 200 `ok`
    ctx.send({
      jsonPaths,
      sbtpCardCount
    });
  }
};
