# Strapi plugin precious-data

A plugin which imports card data from [precious-data](https://github.com/insanity54/precious-data)


## Installation

  1. Add clone this repo to the plugins/ dir in your strapi project. `git clone https://gitlab.com/insanity54/strapi-plugin-precious-data plugins/precious-data`
