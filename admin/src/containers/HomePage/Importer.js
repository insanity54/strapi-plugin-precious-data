/*
 *
 * HomePage data fetching
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getRequestURL } from '../../utils';
import { request } from 'strapi-helper-plugin'
import Info from './Info'

const Importer = ({ }) => {
  const abortController = new AbortController();
  const { signal } = abortController;

  const [ jsonPaths, setJsonPaths ] = useState(0)
  const [ sbtpCardCount, setSbtpCardCount ] = useState(0)

  useEffect(() => {
    const getData = async () => {
      try {
        const { ok } = await request(getRequestURL('import'), { method: 'GET', signal });

      } catch (e) {
        console.log(e)
      }
    }
    getData()
  }, [])

  // if () // @todo display "loading..." if request is in flight
  return <Info sbtpCardCount={sbtpCardCount} prememoCardCount={jsonPaths}></Info>
};


export default memo(Importer);
