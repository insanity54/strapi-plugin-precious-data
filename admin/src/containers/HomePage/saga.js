import { takeLatest } from "redux-saga/effects";
import { request } from 'strapi-helper-plugin'
import { getRequestURL } from '../../utils';


function* initiateImport() {
  try {
    const results = yield call(request, getRequestURL('import'))
  } catch (e) {

  }
}

function* mySaga() {
  yield takeEvery("PREMEMO_IMPORT_REQUESTED", initiateImport);
}

export default mySaga;
