/*
 *
 * HomePage data fetching
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getRequestURL } from '../../utils';
import { request } from 'strapi-helper-plugin'
import Info from './Info'

const HomePage = ({ }) => {
  const abortController = new AbortController();
  const { signal } = abortController;

  const [ jsonPaths, setJsonPaths ] = useState(0)
  const [ sbtpCardCount, setSbtpCardCount ] = useState(0)

  useEffect(() => {
    const getData = async () => {
      try {
        const { jsonPaths: json, sbtpCardCount: sbtp } = await request(getRequestURL('paths'), { method: 'GET', signal });
        setJsonPaths(json.length)
        setSbtpCardCount(sbtp)
      } catch (e) {
        console.log(e)
      }
    }
    getData()
  }, [])

  // if () // @todo display "loading..." if request is in flight
  return <Info sbtpCardCount={sbtpCardCount} prememoCardCount={jsonPaths}></Info>
};


export default memo(HomePage);
