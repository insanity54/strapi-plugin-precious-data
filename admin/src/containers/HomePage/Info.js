/*
 *
 * HomePage Display
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import pluginId from '../../pluginId';
import StartButton from './StartButton';

const Info = ({ sbtpCardCount, prememoCardCount }) => {

return (
    <div>
      <h1>{pluginId} Importer Plugin</h1>
      <p>Number of Prememo cards loaded into sbtp.xyz db: <strong>{sbtpCardCount}</strong></p>
      <p>Number of cards which are known to precious-data: <strong>{prememoCardCount}</strong></p>
      <StartButton />
    </div>
  );
}

Info.propTypes = {
  sbtpCardCount: PropTypes.number.isRequired,
  prememoCardCount: PropTypes.number.isRequired
}


export default memo(Info);
