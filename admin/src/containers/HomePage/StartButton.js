/*
 *
 * HomePage Display
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import pluginId from '../../pluginId';
import { Button, LoadingBar } from '@buffetjs/core'


const StartButton = ({ }) => {

let [ isImporting, setIsImporting ] = useState(false)

if (isImporting) return <LoadingBar />

return (
    <div>
      <Button
        label={'Start Taco Import'}
        onClick={() => {
          console.log(`clicked the button with isImporting at ${isImporting}`)
          setIsImporting(!isImporting)
        }}
        isLoading={isImporting}
      />
    </div>
  );
}

StartButton.propTypes = {
}


export default memo(StartButton);
