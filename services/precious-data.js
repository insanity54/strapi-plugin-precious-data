'use strict';

/**
 * precious-data.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

const path = require('path')
const globby = require('globby')

module.exports = {
  getJsonPaths: async () => {
    let paths = await globby(path.join(__dirname, '../node_modules/precious-data/data/**/*.json'))
    return paths
  },
  getJpgPaths: async () => {
    let paths = await globby(path.join(__dirname, '../node_modules/precious-data/data/**/*.jpg'))
    return paths
  },
  getSbtpCardCount: async () => {
    return strapi.query('prememo-card').count();
  }
};
